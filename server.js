const express = require('express')
const cors = require('cors')
const db = require('./db')
const utils = require('./utils')

const app = express()

app.use(cors('*'))

app.use(express.json())

app.get('/get',(request,response)=>{
    const connection = db.openconnection()


    const statement = `select empid,name,salary,age from Emp`

    connection.query(statement,(error,records)=>{
        connection.end()

        response.send(utils.createResult(error,records))

    })
})

app.post('/add',(request,response)=>{
    const connection = db.openconnection()

    const {name,salary,age}=request.body

    const statement = `insert into Emp (name,salary,age) 
    values ('${name}','${salary}','${age}')`

    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))

    })
})

app.put('/update',(request,response)=>{
    const connection = db.openconnection()

    const {empid,salary}=request.body

    const statement = `update Emp set salary = '${salary}' where empid='${empid}'`

    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))

    })
})

app.delete('/delete/:empid',(request,response)=>{
    const connection = db.openconnection()

    const {empid}=request.params

    const statement = `delete from Emp where empid='${empid}'`

    connection.query(statement,(error,result)=>{
        connection.end()
        response.send(utils.createResult(error,result))

    })
})


app.listen(4000,'0.0.0.0',()=>{
    console.log(`server started on port 4000`)
})